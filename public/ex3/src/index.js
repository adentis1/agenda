
class ClassComponent extends React.Component {
    //Principal
    constructor(props) {
        super(props);
        console.log("Contructor " + props.name);
        this.state = {
            inputText: ''
        }
    }
    //Principal
    componentDidMount() {
        console.log("componentDidMount " + this.props.name);
    }


    //Isto faz com que voltar a entrar no componentDidMount e não deveria, mas nunca utilizei isto na vida,
    // por isso se alguém precisar é porque já está a fazer alguma coisa mal
    shouldComponentUpdate(nextProps, nextState) {
        console.groupCollapsed("shouldComponentUpdate " + this.props.name);
        console.log(nextProps);
        console.log(nextState);
        console.groupEnd();
        return true; // if is to rerender
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        console.groupCollapsed("componentDidUpdate " + this.props.name);
        console.log(prevProps);
        console.log(prevState);
        console.log(snapshot);
        console.groupEnd();
    }

    //Principal
    componentWillUnmount() {
        console.log("componentWillUnmount " + this.props.name);
    }

    handleChangeInput = (ev) => {
        this.setState({ inputText: ev.target.value });
    }

    //Principal
    render() {
        console.log("Render " + this.props.name);
        return (

            <React.Fragment>
                <h3>Class Component {this.props.name}</h3>
                <input type="text" value={this.state.inputText} onChange={this.handleChangeInput}></input>
            </React.Fragment>
        )
    }
}

class Container extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            classComponentOpen: 1
        }
    }

    handleClick = (tab) => {
        this.setState({ classComponentOpen: tab });
    }

    render() {
        console.log("Render Container")
        return (
            <React.Fragment>
                <button onClick={() => this.handleClick(1)}>Class 1</button>
                <button onClick={() => this.handleClick(2)}>Class 2</button>
                {this.state.classComponentOpen === 1 && <ClassComponent name="ClassComponent1" />}
                {this.state.classComponentOpen === 2 && <ClassComponent name="ClassComponent2" />}

            </React.Fragment>
        );
    }


}


const root = document.getElementById('root');
ReactDOM.render(<Container />, root)