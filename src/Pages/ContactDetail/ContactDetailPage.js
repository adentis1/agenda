/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom"
import { getContact } from '../../api/contactsApi';
// import Buttuon from '../../Components/Button/Buttuon';
import ContactDetail from '../../Components/ContactDetail/ContactDetail';

const ContactDetailPage = () => {
    const { id } = useParams();
    const [contact, setContact] = useState({ name: { first: 'Micael', last: 'Costa' }, email: 'teste@email.com' });
    // const [teste, setTeste] = useState(false);
    useEffect(() => {
        //console.log('teste');
        getContact(id).then(res => setContact(res));
    }, []);

    if (contact) {
        return (
            <>
                {/* <Buttuon onClick={() => setTeste(!teste)}>Teste</Buttuon> */}
                <ContactDetail
                    contact={contact}
                />
            </>
        );
    }
    return (<div>Contacto não encontrado</div>)
}

export default ContactDetailPage
