import React from 'react'
import { Link } from 'react-router-dom'
import './Examples.css'

const ExamplesPage = ({ match }) => {
    return (
        <>
            <h2>HTML estático</h2>
            <ul className="exampleWrap">
                <li className="link"><a target="_blank" href="/ex1/index.html">Uso de react em html ja existente</a></li>
                <li className="link"><a target="_blank" href="/ex2/index.html">Functional vs class component</a></li>
                <li className="link"><a target="_blank" href="/ex3/index.html">Lyfe Cycle</a></li>
            </ul>
            <h2>React</h2>
            <ul className="exampleWrap orderedList">
                <li className="link"><Link to={`${match.url}/Ex1/CreateReactComponent`} >Criar primeiro componente</Link></li>
                <li className="link"><Link to={`${match.url}/Ex2/ClassComponentVsFunctionalComponent`} >Class Component vs Functional Component</Link></li>
                <li className="link"><Link to={`${match.url}/Ex3/Props`} >Props</Link></li>
                <li className="link"><Link to={`${match.url}/Ex4/State`} >State</Link></li>
                <li className="link"><Link to={`${match.url}/Ex5/PropsAndState`} >Props and State together</Link></li>
                <li className="link"><Link to={`${match.url}/Ex6/Fragment`} >Fragment</Link></li>
                <li>
                    Context
                    <ul className="orderedList">
                        <li className="link"><Link to={`${match.url}/Ex7/Context/Ex1`} >Context normal</Link></li>
                        <li className="link"><Link to={`${match.url}/Ex7/Context/Ex2`} >Context with abstraction of provider and consumer</Link></li>
                        <li className="link"><Link to={`${match.url}/Ex7/Context/Ex3`} >Context with abstration on Provider and with hooks in consumer</Link></li>
                    </ul>
                </li>
                <li className="link"><Link to={`${match.url}/Ex8/CRUD`} >CRUD</Link></li>
            </ul>
        </>
    )
}

export default ExamplesPage
