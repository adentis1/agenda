import React, { useState, useEffect } from 'react'
import { getFavoriteContactsEndpoint } from '../../api/contactsApi';
import ContactList from './Components/ContactList';

const FavoritesContactPage = (props) => {
    const [favorites, setFavorites] = useState([]);
    const [actionOpen, setActionOpen] = useState(null);

    useEffect(() => {
        getFavoriteContactsEndpoint().then(res => setFavorites(res));
    }, [])

    const hangleAction = (id) => {
        if (id !== actionOpen) {
            setActionOpen(id);
        }
        else {
            setActionOpen(null);
        }
    }

    return (
        <ContactList
            {...props}
            contacts={favorites.map(favorite => {
                const { name, picture, id } = favorite;
                const { first, last } = name;
                const { medium } = picture
                return {
                    id,
                    name: `${first} ${last}`,
                    image: medium

                }
            })}
            actionsOpen={actionOpen}
            onclickContact={hangleAction}
            header="Lista de Contactos"
        />
    )
}

export default FavoritesContactPage
