import React, { useEffect, useState } from "react";
import Modal from "../../../Components/Modal/Modal";
import Button, { ButtonType } from '../../../Components/Button/Buttuon';
import Input from '../../../Components/Input/Input';
import './AddOrEditContact.css';
const AddContact = ({ edit = false, user, onModalClose, onSubmit }) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    useEffect(() => {
        if (edit) {
            setFirstName(user.name.first);
            setLastName(user.name.last);
            setEmail(user.email);
        }
    }, []);

    const renderActions = () => {
        return (<Button buttonType={ButtonType.INFO} onClick={() => onSubmit({ name: { first: firstName, last: lastName }, email })} >{edit ? 'Editar' : "Adicionar"}</Button >);
    }
    return (
        <Modal onClose={onModalClose}
            title="Teste"
            actions={renderActions()}
        >
            <div className="addOrEdit-Wrap">
                <div>
                    <label>Primeiro nome</label>
                    <Input value={firstName} onChange={ev => setFirstName(ev.target.value)} />
                </div>
                <div>
                    <label>Ultimo nome</label>
                    <Input value={lastName} onChange={ev => setLastName(ev.target.value)} />
                </div>
                <div>
                    <label>Email</label>
                    <Input value={email} onChange={ev => setEmail(ev.target.value)} />
                </div>
            </div>
        </Modal>

    )
}

export default AddContact
