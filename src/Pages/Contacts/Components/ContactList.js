import React, { useState } from 'react';
import ContactItem from './ContactItem';
import './contactList.css';
import { Route } from 'react-router-dom';
import Input from '../../../Components/Input/Input';

const ContactList = ({ contacts = [], actionsOpen, onclickContact, match, ...rest }) => {
    const renderContact = (props, index) => (
        <li key={index}>
            <ContactItem {...props} actionsOpen={actionsOpen === props.id} onclickContact={onclickContact} {...rest} />
        </li>
    );
    const [filterValue, setFilterValue] = useState('');
    const handleChangeFilter = (ev) => {
        setFilterValue(ev.target.value);
    }

    const filteredContacts = filterValue ? contacts.filter(contact => contact.name.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1) : contacts;
    return (
        <>
            <ul className="contact-list">
                <Input value={filterValue} onChange={handleChangeFilter} placeholder="Search" />
                <Route path={`${match.url}/Teste`}>
                    <p style={{ backgroundColor: 'red' }}>Teste</p>
                </Route>
                {filteredContacts.map(renderContact)}
            </ul>

        </>
    );
}
export default ContactList
