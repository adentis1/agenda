import React from 'react';
import { Link } from 'react-router-dom';
import IconButton from '../../../Components/Button/IconButton';
import InfoIcon from '../../../Components/Icons/InfoIcon';
import MessagesIcon from '../../../Components/Icons/MessagesIcon';
import UserEditIcon from '../../../Components/Icons/UserEditIcon'

export default function ContactActions({ contactId, onEditContact }) {
    return (
        <div className="contact-actions">
            <IconButton onClick={() => console.log('teste Message Icon')} >
                <MessagesIcon />
            </IconButton>
            <Link to={`/contact/${contactId}`}>
                <InfoIcon />
            </Link>
            <IconButton onClick={() => onEditContact(contactId)} >
                <UserEditIcon />
            </IconButton>
        </div>
    )
}
