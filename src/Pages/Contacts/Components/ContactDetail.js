import React from 'react'
import UserContext from '../../../Context/UserContext'

const ContactDetail = props => {
    return (
        <UserContext.Consumer>
            {
                (context) => {
                    console.log(context);
                    return (
                        <div>{context.test.a}</div>
                    )
                }
            }
        </UserContext.Consumer>
    )
}
export default ContactDetail
