import React, { Component } from 'react';
import ContactList from './Components/ContactList';
import { getContacts, uuidv4 } from '../../api/contactsApi';
import Buttuon from '../../Components/Button/Buttuon';
import UserContext from '../../Context/UserContext';
import UserProvider from '../../Context/UserContextProvider'
import ComponentUsingWith from './UseContext/ComponentUsingWith';
import ComponentNormal from './UseContext/ComponentNormal'
import './contactPage.css'

import AddContact from './Components/AddOrEditContact';
class ContactsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: [],
            actionOpen: null,
            modalOpen: false,
            ContactIdToEdit: null
        }
    }

    // const [actionOpen, setActionOpen] = useState(null);

    componentDidMount() {
        console.log('ContactsPage request');
        getContacts().then(contacts => {
            contacts = contacts.sort((a, b) => a.name.first.toUpperCase() > b.name.first.toUpperCase() ? 1 : -1)
            this.setState({ contacts })
        });
    }

    hangleAction = (id) => {
        const { actionOpen } = this.state;
        if (id !== actionOpen) {
            this.setState({ actionOpen: id });
        }
        else {
            this.setState({ actionOpen: null });
        }
    }

    changeUserName = (id, name) => {
        const newUsers = this.state.users.map(user => {
            if (user.id === id) {
                return { ...user, name }
            }
            return user;
        });
        console.log(newUsers);
        this.setState({ ...this.state, users: newUsers })
    }

    onCloseModal = () => {
        this.setState({ modalOpen: false, ContactIdToEdit: null });
    }

    _onSubmitContact = (contactParam) => {
        const { ContactIdToEdit, contacts } = this.state;
        if (!ContactIdToEdit) {
            this.setState({ contacts: [...this.state.contacts, { ...contactParam, id: uuidv4(), picture: {} }], modalOpen: false, contactId: null });
        } else {
            const newContacts = contacts.map(contact => {
                if (ContactIdToEdit === contact.id) {
                    return {
                        ...contact, ...contactParam
                    };
                }
                return contact;
            });
            this.setState({ contacts: newContacts, modalOpen: false, ContactIdToEdit: null });
        }
    }


    handleCreateUser = () => {
        this.setState({ modalOpen: true });
    }

    onEditContact = (contactId) => {
        this.setState({ modalOpen: true, ContactIdToEdit: contactId });
    }

    render() {
        const { contacts, actionOpen, users, modalOpen, ContactIdToEdit } = this.state;
        const context = {
            users,
            changeUserName: this.changeUserName
        };
        return (
            <>
                {/* 
                <UserContext.Provider value={context}>
                    <div>Context Normal Provider</div>
                    <ComponentNormal />
                    <ComponentUsingWith />
                </UserContext.Provider>

                <UserProvider users={users} changeUserName={this.changeUserName}>
                    <div>Context abstration Provider</div>
                    <ComponentNormal />
                    <ComponentUsingWith />
                </UserProvider> */}


                <div className="contact-header">
                    <h3>Lista de Contactos</h3>
                    <Buttuon style={{ fontSize: '15px' }} onClick={this.handleCreateUser}>+</Buttuon>
                </div>
                <ContactList
                    {...this.props}
                    contacts={contacts.map(contact => {
                        const { name, picture, id } = contact;
                        const { first, last } = name;
                        const { medium } = picture
                        return {
                            id,
                            name: `${first} ${last}`,
                            image: medium

                        }
                    })}
                    actionsOpen={actionOpen}
                    onclickContact={this.hangleAction}
                    onCreateUser={this.onCreateUser}
                    onEditContact={this.onEditContact}
                />
                {
                    modalOpen && <AddContact user={ContactIdToEdit !== null ? contacts.find(x => x.id === ContactIdToEdit) : null} edit={ContactIdToEdit !== null} onModalClose={this.onCloseModal} onSubmit={this._onSubmitContact} />
                }
            </>

        );
    }

}

export default ContactsPage;
