import React, { useState } from 'react'
import UserContext from '../../../Context/UserContext';


const User = ({ id, name }) => {

    const [inputValue, setInputValue] = useState('');

    const handleOnChange = (ev) => {
        setInputValue(ev.target.value);
    }

    const handleChangeUser = (changeUserName) => {
        changeUserName(id, inputValue);
    }

    return (
        <>
            <UserContext.Consumer>
                {
                    (context) => (
                        <>
                            <div>{name}</div>
                            <input value={inputValue} onChange={handleOnChange} />
                            <button onClick={() => handleChangeUser(context.changeUserName)}>Change Name</button>
                        </>
                    )
                }
            </UserContext.Consumer>
        </>
    )
}



const ComponentNormal = () => {
    return (
        <UserContext.Consumer>
            {
                (context) => {
                    return context.users.map((user, index) => (
                        <User key={`Users${index}`} id={user.id} name={user.name} />
                    ))
                }

            }
        </UserContext.Consumer>
    )
}

export default ComponentNormal;
