import React, { useState } from 'react'
import WithUserContext from '../../../Context/WithUserContext';

const User = ({ id, name, userContext }) => {

    const [inputValue, setInputValue] = useState('');

    const handleOnChange = (ev) => {
        setInputValue(ev.target.value);
    }

    const handleChangeUser = (changeUserName) => {
        userContext.changeUserName(id, inputValue);
    }

    return (
        <>
            <div>{name}</div>
            <input value={inputValue} onChange={handleOnChange} />
            <button onClick={handleChangeUser}>Change Name</button>
        </>
    )
}
const UserWrap = WithUserContext(User);

const ComponentUsingWith = ({ userContext }) => {
    return userContext.users.map((user, index) => (
        <UserWrap key={`Users${index}`} id={user.id} name={user.name} />
    ))
}

export default WithUserContext(ComponentUsingWith);