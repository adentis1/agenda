import React from 'react'
import { UserContextProvider } from './UserContext'

const UserProvider = ({ children, ...propsToContext }) => {
    return (
        <UserContextProvider value={propsToContext}>
            {children}
        </UserContextProvider>
    );
};

export default UserProvider;