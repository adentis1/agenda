import { createContext } from 'react';



const ContactDetailContext = createContext(null);
export default ContactDetailContext;

export const ContactDetailContextProvider = ContactDetailContext.Provider;
export const ContactDetailContextConsumer = ContactDetailContext.Consumer;