import { createContext } from 'react';

const initialContext = {
    user: 'User Test'
}

const UserContext = createContext(initialContext);
export default UserContext;

export const UserContextProvider = UserContext.Provider
export const UserContextConsumer = UserContext.Consumer