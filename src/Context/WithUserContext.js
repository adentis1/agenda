import React from 'react'
import { UserContextConsumer } from './UserContext';


const WithUserContext = (WrappedComponent) => (props) => (
    <UserContextConsumer>
        {
            (propsToContext) =>
                <WrappedComponent {...props} userContext={propsToContext} />
        }
    </UserContextConsumer >
);

export default WithUserContext;