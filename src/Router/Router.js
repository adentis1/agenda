import React, { Component } from 'react'
import { Redirect, Route, BrowserRouter, Switch } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { getContacts } from '../api/contactsApi';
import ContactList from '../Pages/Contacts/Components/ContactList';
import Header from '../Components/Header/Header';
import ContactDetailPage from '../Pages/ContactDetail/ContactDetailPage';
import ContactDetail from '../Components/ContactDetail/ContactDetail';

//https://reactrouter.com/web/api/Route
//https://ui.dev/react-router-v4-nested-routes/
const history = createMemoryHistory({ initialEntries: ['/'] })

export default class Router extends Component {

    constructor(props) {
        super(props);
        this.state = {
            contacts: [],
            actionOpen: null
        }
    }

    // const [actionOpen, setActionOpen] = useState(null);

    componentDidMount() {
        console.log('Router request');
        getContacts().then(contacts => {
            contacts = contacts.sort((a, b) => a.name.first.toUpperCase() > b.name.first.toUpperCase() ? 1 : -1)
            // setTimeout(() => 
            this.setState({ contacts });
            // , 5000);
        });
    }

    hangleAction = (id) => {
        const { actionOpen } = this.state;
        if (id !== actionOpen) {
            this.setState({ actionOpen: id });
        }
        else {
            this.setState({ actionOpen: null });
        }
    }

    getFavorites = (contacts) => {
        const favorites = contacts.filter(contact => contact.favorite).map(contact => {
            const { name, picture, id } = contact;
            const { first, last } = name;
            const { medium } = picture
            return {
                id,
                name: `${first} ${last}`,
                image: medium
            }
        });
        return favorites;
    }
    render() {
        const { contacts, actionOpen } = this.state;
        return (
            <BrowserRouter history={history}>
                <Header />
                <div className="content">
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/ContactList" />
                        </Route>
                        <Route path="/ContactList" render={
                            (props) => (
                                <ContactList
                                    {...props}
                                    contacts={contacts.map(contact => {
                                        const { name, picture, id } = contact;
                                        const { first, last } = name;
                                        const { medium } = picture
                                        return {
                                            id,
                                            name: `${first} ${last}`,
                                            image: medium

                                        }
                                    })}
                                    actionsOpen={actionOpen}
                                    onclickContact={this.hangleAction}
                                    header="Lista de Contactos"
                                />
                            )}
                        />
                        <Route path="/Favorites"
                            // component={ContactList}
                            render={(props) => (
                                <ContactList
                                    {...props}
                                    contacts={this.getFavorites(contacts)}
                                    actionsOpen={actionOpen}
                                    onclickContact={this.hangleAction}
                                    header="Lista de Contactos Favoritos"
                                />
                            )}
                        >
                        </Route>
                        <Route path="/Contact/:id"
                            render={(props) => {
                                const contact = this.state.contacts.find(contact => contact.id === props.match.params.id);
                                if (contact) {
                                    return (
                                        <ContactDetail
                                            contact={contact}
                                        />
                                    )
                                }
                                return (<div>Contacto não encontrado</div>)
                            }}
                        />
                    </Switch>
                </div>
            </BrowserRouter >
        )
    }
}
