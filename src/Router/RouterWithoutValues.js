import React from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Header from '../Components/Header/Header';
import { CRUDContainer, ClassComponentVsFunctionalComponentContainer, ContextContainerEx1, ContextContainerEx2, ContextContainerEx3, CreateComponent, FragmentContainer, PropsAndStateContainer, PropsContainer, StateContainer } from '../Examples/';
import ContactDetailPage from '../Pages/ContactDetail/ContactDetailPage';
import ContactsPage from '../Pages/Contacts/ContactsPage'
import FavoritesContactPage from '../Pages/Contacts/FavoritesContactPage'
import ExamplesPage from '../Pages/Examples/ExamplesPage';

const RouterWithoutValues = () => {
    return (
        <BrowserRouter>
            <Header />
            <div className="content">
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/ContactList" />
                    </Route>
                    <Route path="/ContactList" component={ContactsPage} />
                    <Route path="/Favorites" component={FavoritesContactPage} />
                    <Route path="/Contact/:id" component={ContactDetailPage} />
                    {/* se remover o exact o componente CreateComponent não será apresentado */}
                    <Route exact path="/Examples" component={ExamplesPage} />
                    <Route path="/Examples/Ex1/CreateReactComponent" component={CreateComponent} />
                    <Route path="/Examples/Ex2/ClassComponentVsFunctionalComponent" component={ClassComponentVsFunctionalComponentContainer} />
                    <Route path="/Examples/Ex3/Props" component={PropsContainer} />
                    <Route path="/Examples/Ex4/State" component={StateContainer} />
                    <Route path="/Examples/Ex5/PropsAndState" component={PropsAndStateContainer} />
                    <Route path="/Examples/Ex6/Fragment" component={FragmentContainer} />
                    <Route path="/Examples/Ex7/Context/Ex1" component={ContextContainerEx1} />
                    <Route path="/Examples/Ex7/Context/Ex2" component={ContextContainerEx2} />
                    <Route path="/Examples/Ex7/Context/Ex3" component={ContextContainerEx3} />
                    <Route path="/Examples/Ex8/CRUD" component={CRUDContainer} />
                </Switch>
            </div>
        </BrowserRouter >
    )
}

export default RouterWithoutValues
