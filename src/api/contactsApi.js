import data from './ContactDataMock.json';
export const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const mapContacts = (users) => {
    return users.map(user => {
        const { name, phone, picture, email, location } = user;
        return {
            name,
            phone,
            picture,
            email,
            location,
            id: uuidv4(),
            favorite: Math.round(Math.random())
        }
    });
}
//Usado de forma genérica no router com render
export const getContacts = () => {

    // return fetch('https://randomuser.me/api/?results=20')
    //     .then(res => res.json())
    //     .then(res => {
    //         return mapContacts(res.results);
    //     });
    // console.log('teste');
    return new Promise((resolve, reject) => {

        const result = data.map(contact => ({ ...contact, name: contact.nome }));
        resolve(result);
    });
}

export const getContactsEndpoint = () => {
    // Usei apenas o mesmo método como exemplo, mas aqui seria uma chamada a um endpoit para retornar apenas contactos sem os favoritos
    // até pode trazer os favoritos, mas isso é decisão da api. Nós vamos fazer a diferenciação porque temos que fazer uma chamada em cada componente
    return getContacts();
}

export const getFavoriteContactsEndpoint = () => {
    return getContacts()
        //Este then podia ser utilizado também numa situação do endpoint ser o mesmo para os contacts e favoritos
        // Contudo estou a usar aqui porque estamos a fazer um mapContacts e aí é que é gerado os favoritos
        .then(res => res.filter(contact => contact.favorite));
}

export const getContact = async (id) => {
    const contacts = await getContacts();
    return contacts.find(contact => contact.id === id);
}

