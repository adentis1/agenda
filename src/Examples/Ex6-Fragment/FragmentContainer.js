import React /*, { Fragment } */ from 'react'

function FragmentContainer() {
    return (
        <> {/* Eu sou um container e não irei aparecer no html. 
                Posso trambém ser representado como <Fragment> ou <React.Fragment> sendo que na primeira 
                situação tenho que importar o Fragment do React.
                Sou útil para não criar tags desnecessárias no html
                */}
            <p>Teste</p>
            <p>Necessito de ter uma tag pai porque o return pode apenas trazer um container</p>
        </>
    )
}

export default FragmentContainer
