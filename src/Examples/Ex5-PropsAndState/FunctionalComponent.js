import React from 'react'

//Ao contrário do setState do class component, este não sobrepõe o primeiro nível do objecto
const FunctionalComponent = ({ name, lastName, onChangeName, onChangeLastName }) => {

    const handleNameClick = () => {
        onChangeName('Functional Name');
    }

    return (
        <div>
            <h1>Eu sou um functional component</h1>
            <p><b>Name: </b>{name}</p>
            <p><b>Last Name: </b>{lastName}</p>
            <button onClick={handleNameClick}>Change Name</button>
            <button onClick={() => onChangeLastName('Functional Last Name')}>Change Last Name </button>

        </div>
    );
}

export default FunctionalComponent
