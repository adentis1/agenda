import React, { Component } from 'react'

class ClassComponent extends Component {
    handleNameClick = () => {
        const { onChangeName } = this.props;
        onChangeName('Class Name');
    }
    render() {
        const { onChangeLastName, name, lastName } = this.props;
        return (
            <div>
                <h1>Eu sou um functional component</h1>
                <p><b>Name: </b>{name}</p>
                <p><b>Last Name: </b>{lastName}</p>
                <button onClick={this.handleNameClick}>Change Name</button>
                <button onClick={() => onChangeLastName('Class Last Name')}>Change Last Name </button>
            </div>
        );
    }

}

export default ClassComponent
