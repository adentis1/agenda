import React, { useState } from 'react'
import ClassComponent from './ClassComponent'
import FunctionalComponent from './FunctionalComponent'


const PropsAndStateContainer = () => {

    const [state, setState] = useState({
        nome: '',
        sobrenome: ''
    });

    const changeName = (newName) => {
        setState({ ...state, nome: newName });
    }

    const changeLastName = (newLastName) => {
        setState({ ...state, sobrenome: newLastName });
    }

    return (
        <>
            <FunctionalComponent
                name={state.nome}
                lastName={state.sobrenome}
                onChangeName={changeName}
                onChangeLastName={changeLastName}
            />
            <ClassComponent
                name={state.nome}
                lastName={state.sobrenome}
                onChangeName={changeName}
                onChangeLastName={changeLastName}
            />
        </>
    )
}

export default PropsAndStateContainer

