import React, { useState } from 'react'

//Ao contrário do setState do class component, este não sobrepõe o primeiro nível do objecto
const AddOrEdit = ({ onSubmit }) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');

    return (
        <>
            <div>
                <label>Nome</label>
                <input value={name} onChange={ev => setName(ev.target.value)} />
            </div>
            <div>
                <label>Email</label>
                <input value={email} onChange={ev => setEmail(ev.target.value)} />
            </div>
            <button onClick={() => onSubmit({ name, email })}>Add</button>
        </>
    );
}

export default AddOrEdit
