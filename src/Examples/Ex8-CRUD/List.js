import React from 'react'


const List = ({ users }) => {

    return (
        <>
            <h2>Lista de utilizadores</h2>
            <ul>
                {users.map(user => (<li key={user.id}>{`${user.name} - ${user.email}`}</li>))}
            </ul>
        </>
    )
}

export default List

