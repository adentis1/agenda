import React, { Component } from 'react'
import AddOrEdit from './AddOrEdit';
import { getUsers, uuidv4 } from './Api/UsersApi';
import List from './List';

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            addOrEdit: false
        }
    }

    componentDidMount() {
        getUsers().then(users => {
            this.setState({ users });
        })
    }
    addUser = () => {
        this.setState({ addOrEdit: true });
    }

    onEditItem = () => {

    }

    onSubmit = (user) => {
        const { users } = this.state;
        var newId = uuidv4();
        this.setState({ users: [...users, { ...user, id: newId }], addOrEdit: false });
    }

    render() {
        const { users, addOrEdit } = this.state;
        return (
            <>
                <button onClick={this.addUser}>add</button>
                {addOrEdit && <AddOrEdit onSubmit={this.onSubmit} />}
                <List users={users} onEditItem={this.onEditItem} />
            </>
        );
    }

}

export default Container
