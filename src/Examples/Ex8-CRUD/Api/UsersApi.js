export const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const users = [{
    id: uuidv4(),
    name: "Micael",
    email: "micael@test.pt"
},
{
    id: uuidv4(),
    name: "João",
    email: "joao@test.pt"
}];
export const getUsers = () => {
    return Promise.resolve(users);
}