import React from 'react'
import ClassComponent from './ClassComponent'
import FunctionalComponent from './FunctionalComponent'




const PropsContainer = () => {

    const anyObject = {
        propriedade1: 'teste',
        propriedade2: 'Teste2'
    };

    const anyFunction = () => {
        console.log('anyFuncstion');
    }

    return (
        <>
            <FunctionalComponent
                name="Micael"
                otherProp="Outra propriedade"
                functionProp={anyFunction}
                objectProp={anyObject}
            >
                <h1>Sou o atributo children dentro o objecto props</h1>
            </FunctionalComponent>
            <ClassComponent
                name="Micael"
                otherProp="Outra propriedade"
                functionProp={anyFunction}
                objectProp={anyObject}
            >
                <h1>Sou o atributo children dentro o objecto props</h1>

            </ClassComponent>
        </>
    )
}

export default PropsContainer
