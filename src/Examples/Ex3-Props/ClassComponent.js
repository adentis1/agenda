import React, { Component } from 'react'


class ClassComponent extends Component {

    render() {

        const { name, otherProp, functionProp, objectProp, children } = this.props;
        // const name = this.props.name;
        // const otherProp = this.props.otherProp;
        const { propriedade1, propriedade2 } = objectProp;

        return (
            <div>
                <h1>Class Component</h1>
                <p><b>Name: </b>{name}</p>
                <p><b>otherProp: </b>{otherProp}</p>
                <h2>Propriedade do objecto</h2>
                <p><b>propriedade1: </b>{propriedade1}</p>
                <p><b>propriedade2: </b>{propriedade2}</p>
                <button onClick={functionProp}>Click Me and open console to see the result</button>
                <p>Sou um class component</p>
                <div style={{ color: 'red' }}>{children}</div>
            </div>
        )
    }
}

export default ClassComponent
