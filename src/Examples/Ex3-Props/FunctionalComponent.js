import React from 'react'

// const FunctionalComponent = ({ name, otherProp, functionProp, objectProp }) => {
const FunctionalComponent = (props) => {
    const { name, otherProp, functionProp, objectProp, children } = props;
    const { propriedade1, propriedade2 } = objectProp;
    return (
        <div>
            <h1>FunctionalComponent</h1>
            <p><b>Name: </b>{name}</p>
            <p><b>otherProp: </b>{otherProp}</p>
            <h2>Propriedade do objecto</h2>
            <p><b>propriedade1: </b>{propriedade1}</p>
            <p><b>propriedade2: </b>{propriedade2}</p>
            <button onClick={functionProp}>Click Me and open console to see the result</button>
            <p>Sou um functional component</p>
            <div style={{ color: 'red' }}>{children}</div>
        </div>
    )
}

export default FunctionalComponent
