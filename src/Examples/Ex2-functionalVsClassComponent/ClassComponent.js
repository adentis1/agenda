import React, { Component } from 'react'


/*
O render() retorna o HTML (JSX)
Extends React.Component
para acessar props usamos o this

Contem vários eventos, tais como 
construtor 
componentDidMount - entra neste método apenas uma vez durante todo o ciclo de vida do componente e ocorre depois do primeiro render
shouldComponentUpdate - entra neste método sempre que um componente necessita de ser rederizado e serve para indicar se o componente deve ou não ser renderizado com base em determinada condição
componentDidUpdate - é chamado depois do render, ou seja a UI já foi atualizada e é o local ideal para fazer chamadas a webapi's com base nas props que foram atualizadas
componentWillUnmount - é chamado sempre que o componente não é mais usado, ou seja, é chamado antes de destruir o componente. pode ser util para parar timers
*/
class ClassComponent extends Component {
    //Principal
    constructor(props) {
        super(props);
        console.log("Contructor ");
        this.state = {
            nome: ""
        }

    }
    //Principal
    componentDidMount() {
        console.log("componentDidMount ");
    }

    handleClick = () => {
        // Isto não se faz
        //https://reactjs.org/docs/faq-state.html
        // this.state.nome = "xpto";
        // this.state.idade = 28;
        // this.forceUpdate();

        this.setState({ nome: "xpto", idade: 28 });
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.groupCollapsed("shouldComponentUpdate ");
        console.log(nextProps);
        console.log(nextState);
        console.groupEnd();
        return true; // if is to rerender
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        console.groupCollapsed("componentDidUpdate ");
        console.log(prevProps);
        console.log(prevState);
        console.log(snapshot);
        console.groupEnd();
    }

    //Principal
    componentWillUnmount() {
        console.log("componentWillUnmount ");
    }

    renderButton = () => {
        return (<button>sou um botão</button>)
    }

    render() {
        console.log("render")
        return (
            <div>
                Eu sou um class component
                <div>{this.state.nome}</div>
                <button onClick={this.handleClick}>Click Me</button>
                {this.renderButton()}
            </div>
        )
    }
}

export default ClassComponent
