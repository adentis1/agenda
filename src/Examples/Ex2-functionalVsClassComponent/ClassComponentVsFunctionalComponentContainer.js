import React, { useState } from 'react'
import ClassComponent from './ClassComponent'
import FunctionalComponent from './FunctionalComponent'




// https://www.twilio.com/blog/react-choose-functional-components
const ClassComponentVsFunctionalComponentContainer = () => {
    const [state, setstate] = useState(true);
    const handleClick = () => {
        setstate(!state);
    }
    return (
        <>
            { <FunctionalComponent visible={state} />}
            {!state && <ClassComponent />}
            <button onClick={handleClick}>ChangeComponent</button>
        </>
    )
}

export default ClassComponentVsFunctionalComponentContainer
