import React from 'react'


/*
    O return retorna o HTML (JSX)
    as props são recebidas como parametro da função
    Gestão de estado com o useState só existe depois da versão do React 16.8
    Eventos são geridos com hooks (useEffect)
    Não existe eventos herdados
    
*/



const FunctionalComponent = () => {

    const renderButton = () => {
        return (<button>sou um botão</button>)
    }


    return (
        <div>
            Sou um functional component
            {renderButton()}
        </div>
    )
}

export default FunctionalComponent
