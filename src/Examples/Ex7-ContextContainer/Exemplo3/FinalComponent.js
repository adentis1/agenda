import React from 'react'
import WithTestContext from './Context/WithTestContext';

//Ao contrário do setState do class component, este não sobrepõe o primeiro nível do objecto
const FinalComponent = ({ testeContext }) => {
    return (
        <div>
            <h1>Recebo informação via context usando decorator</h1>
            <p><b>Name: </b>{testeContext.state.nome}</p>
            <p><b>Last Name: </b>{testeContext.state.sobrenome}</p>
            <button onClick={() => testeContext.changeName('Micael')}>Change Name</button>
            <button onClick={() => testeContext.changeLastName('Functional Last Name')}>Change Last Name </button>
        </div>
    );
}

export default WithTestContext(FinalComponent)
