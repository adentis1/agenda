import React from 'react'
import { TesteContextConsumer } from './TesteContext';


const WithTestContext = (WrappedComponent) => (props) => (
    <TesteContextConsumer>
        {
            (contextValue) =>
                <WrappedComponent {...props} testeContext={contextValue} />
        }
    </TesteContextConsumer >
);

export default WithTestContext;