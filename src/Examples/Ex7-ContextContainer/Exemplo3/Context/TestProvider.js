import React from 'react'
import { TesteContextProvider } from './TesteContext'

const TesteProvider = ({ children, ...propsToContext }) => {
    return (
        <TesteContextProvider value={propsToContext}>
            {children}
        </TesteContextProvider>
    );
};

export default TesteProvider;