import { createContext } from 'react';

const context = createContext(null);

export const TesteContextProvider = context.Provider;
export const TesteContextConsumer = context.Consumer;