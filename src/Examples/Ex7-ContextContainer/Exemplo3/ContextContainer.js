import React, { useState } from 'react'
import ComponentChild from './ComponentChild';
import { TesteContextProvider } from './Context/TesteContext';
import TesteProvider from './Context/TestProvider';


const ContextContainer = () => {

    const [state, setState] = useState({
        nome: '',
        sobrenome: ''
    });

    const changeName = (newName) => {
        setState({ ...state, nome: newName });
    }

    const changeLastName = (newLastName) => {
        setState({ ...state, sobrenome: newLastName });
    }

    return (
        <TesteProvider
            state={state}
            changeName={changeName}
            changeLastName={changeLastName}
        >
            <ComponentChild />
        </TesteProvider >
    )
}

export default ContextContainer

