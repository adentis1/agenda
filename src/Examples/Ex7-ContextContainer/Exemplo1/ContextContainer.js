import React, { useState, createContext } from 'react'
import ComponentChild from './ComponentChild';

export const TesteContext = createContext(null);

const ContextContainer = () => {

    const [state, setState] = useState({
        nome: '',
        sobrenome: ''
    });

    const changeName = (newName) => {
        setState({ ...state, nome: newName });
    }

    const changeLastName = (newLastName) => {
        setState({ ...state, sobrenome: newLastName });
    }

    return (
        <>
            <TesteContext.Provider value={{
                state,
                changeName,
                changeLastName
            }}>
                <ComponentChild />

            </TesteContext.Provider>
        </>
    )
}

export default ContextContainer

