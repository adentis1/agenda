import React from 'react'
import { TesteContext } from './ContextContainer';

//Ao contrário do setState do class component, este não sobrepõe o primeiro nível do objecto
const FinalComponent = () => {
    return (
        <TesteContext.Consumer>
            {
                (context) => (
                    <div>
                        <h1>Recebo informação via context</h1>
                        <p><b>Name: </b>{context.state.nome}</p>
                        <p><b>Last Name: </b>{context.state.sobrenome}</p>
                        <button onClick={() => context.changeName('micael')}>Change Name</button>
                        <button onClick={() => context.changeLastName('Functional Last Name')}>Change Last Name </button>
                    </div>
                )
            }
        </TesteContext.Consumer>
    );
}

export default FinalComponent
