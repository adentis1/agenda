import React from 'react'
import FinalComponent from './FinalComponent'

function ComponentChild() {
    return (
        <FinalComponent />
    )
}

export default ComponentChild
