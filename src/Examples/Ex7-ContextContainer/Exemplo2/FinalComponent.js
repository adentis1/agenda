import React from 'react'
import { TesteContextConsumer } from './Context/TesteContext';

//Ao contrário do setState do class component, este não sobrepõe o primeiro nível do objecto
const FinalComponent = () => {
    return (
        <TesteContextConsumer>
            {
                context => (
                    <div>
                        <h1>Recebo informação via context da forma normal, masaaa ccomo   abstração da construç~
                        ao do provider e consummer
                       </h1>
                        <p><b>Name: </b>{context.state.nome}</p>
                        <p><b>Last Name: </b>{context.state.sobrenome}</p>
                        <button onClick={() => context.changeName('micael')}>Change Name</button>
                        <button onClick={() => context.changeLastName('Functional Last Name')}>Change Last Name </button>
                    </div>
                )
            }
        </TesteContextConsumer>
    );
}

export default FinalComponent
