import React, { useState } from 'react'
import ComponentChild from './ComponentChild';
import { TesteContextProvider } from './Context/TesteContext';


const ContextContainer = () => {

    const [state, setState] = useState({
        nome: '',
        sobrenome: ''
    });

    const changeName = (newName) => {
        setState({ ...state, nome: newName });
    }

    const changeLastName = (newLastName) => {
        setState({ ...state, sobrenome: newLastName });
    }

    return (
        <TesteContextProvider value={{
            state,
            changeName,
            changeLastName
        }}>
            <ComponentChild />

        </TesteContextProvider>
    )
}

export default ContextContainer

