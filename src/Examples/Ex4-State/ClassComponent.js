import React, { Component } from 'react'


/*
Podemos ter apenas um único estado
*/
class ClassComponent extends Component {
    //Principal
    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            sobrenome: "",
            otherObj: {
                title: "Sou o titulo da aplicação",
                subtitle: "Sou o sub-titulo da aplicação"
            }
        }
    }

    handleNameClick =() => {
        const newotherObj2 = { title: "asdasdas" };

        this.setState({ nome: '', otherObj2: newotherObj2 });
    }

    handleLastNameClick = () => {
        this.setState({ sobrenome: 'Costa' });
    }
    handleSubTitleClick = () => {
        //Esta linha irá eliminar o subtitulo porque o setState apenas sobrepões propriedades do primeiro nível de um objecto
        this.setState({ otherObj: { title: 'Sou o novo titulo' } });
        //this.setState({ otherObj: { ...this.state.otherObj, title: 'Sou o novo titulo' } });
    }


    render() {
        const { nome, sobrenome, otherObj } = this.state;
        return (
            <div>
                <h1>Eu sou um class component</h1>
                <h2>{otherObj.title}</h2>
                <h3>{otherObj.subtitle}</h3>
                <p><b>Name: </b>{nome}</p>
                <p><b>Last Name: </b>{sobrenome}</p>
                <button onClick={this.handleNameClick}>Change Name</button>
                <button onClick={this.handleLastNameClick}>Change Last Name </button>
                <button onClick={this.handleSubTitleClick}>Change title</button>
            </div>
        )
    }
}

export default ClassComponent
