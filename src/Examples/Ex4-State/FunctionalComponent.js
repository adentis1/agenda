import React, { useState } from 'react'

//Ao contrário do setState do class component, este não sobrepõe o primeiro nível do objecto
const FunctionalComponent = () => {

    const [state, setState] = useState({
        nome: "",
        sobrenome: "",
        otherObj: {
            title: "Sou o titulo da aplicação",
            subtitle: "Sou o sub-titulo da aplicação"
        }
    });

    const [nome, setNome] = useState('');
    const [sobrenome, setSobrenome] = useState('');
    const [title, setTitle] = useState("Sou o titulo da aplicação");
    const [subtitle, setSubTitle] = useState("Sou o sub-titulo da aplicação");


    const handleNameClick = () => {
        //setState({ nome: 'Micael' });
        setNome('Micael');
        setSobrenome('any last name')
    }

    const handleLastNameClick = () => {
        //setState({ sobrenome: 'Costa' });
        setSobrenome('Costa');
    }

    const handleSubTitleClick = () => {
        setTitle('Sou um titulo');
        setSubTitle('Sou um Sub titulo');
    }

    // const { nome, sobrenome, otherObj } = state;
    // const { title, subtitle } = otherObj;
    return (
        <div>
            <h1>Eu sou um functional component</h1>
            <h2>{title}</h2>
            <h3>{subtitle}</h3>
            <p><b>Name: </b>{nome}</p>
            <p><b>Last Name: </b>{sobrenome}</p>
            <button onClick={handleNameClick}>Change Name</button>
            <button onClick={handleLastNameClick}>Change Last Name </button>
            <button onClick={handleSubTitleClick}>Change title</button>

        </div>
    );
}

export default FunctionalComponent
