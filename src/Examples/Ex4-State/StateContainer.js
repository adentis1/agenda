import React from 'react'
import ClassComponent from './ClassComponent'
import FunctionalComponent from './FunctionalComponent'


const StateContainer = () => {
    return (
        <>
            <FunctionalComponent />
            <ClassComponent />
        </>
    )
}

export default StateContainer
