import React from 'react';
import Router from './Router/Router';
import RouterWithoutValues from './Router/RouterWithoutValues';

const App = () => {
    return (
        <>
            {/* {<Router />} */}
            {<RouterWithoutValues />}
        </>
    )
}

export default App
