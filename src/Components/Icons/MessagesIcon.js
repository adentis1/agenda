
import React from 'react'
import Icon from './Icon';
import { ICONS } from './EnumIcons'

const MessagesIcon = ({ width = '1.5rem' }) => {

    return (
        <Icon width={width} icon={ICONS.MESSAGE} />
    )
}

export default MessagesIcon
