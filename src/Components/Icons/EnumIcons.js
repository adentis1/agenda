
import { faCommentAlt, faInfoCircle, faUserEdit } from '@fortawesome/free-solid-svg-icons';

export const ICONS = {
    MESSAGE: faCommentAlt,
    INFO: faInfoCircle,
    USER_EDIT: faUserEdit
}