import React from 'react'
import { ICONS } from './EnumIcons'
import Icon from './Icon'

const InfoIcon = ({ width = '1.5rem' }) => {

    return (
        <Icon className="icon-info" width={width} icon={ICONS.INFO} />
    )
}

export default InfoIcon
