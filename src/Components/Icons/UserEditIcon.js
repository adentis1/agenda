import React from 'react'
import Icon from './Icon';
import { ICONS } from './EnumIcons'

const UserEditIcon = ({ width = '1.5rem' }) => {
    return (
        <Icon width={width} icon={ICONS.USER_EDIT} />
    );
}

export default UserEditIcon
