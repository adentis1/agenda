import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Icon.css'

const Icon = ({ style, width, icon, ...rest }) => {
    return (
        <FontAwesomeIcon className="icon-wrap" style={{ ...style, fontSize: width }} icon={icon} />
    )
}

export default Icon
