
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCommentAlt } from '@fortawesome/free-solid-svg-icons';

const CallIcon = ({ width = '1.5rem' }) => {



    return (
        <FontAwesomeIcon width={width} icon={faCommentAlt} />
    )
}

export default CallIcon;
