import React from 'react'
import './Input.css'

const Input = ({ className, ...rest }) => {
    return (
        <input {...rest} className={[className, 'input-wrap'].join(' ')} />
    )
}

export default Input
