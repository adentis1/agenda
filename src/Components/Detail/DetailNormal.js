import React from 'react'

const DetailNormal = ({ label, value, labelStyle }) => {

    const style = {
        wrap: {
            display: 'flex',
            alignItems: 'center',
            padding: '5px 0'
        },
        label: {
            width: '120px',
            paddingRight: '10px',
            textAlign: 'right',
            fontWeight: 600
        }
    };

    return (
        <div style={style.wrap}>
            <label style={{ ...style.label, ...labelStyle }}>{`${label}:`}</label>
            <div style={style.value}>{value}</div>
        </div >
    )
}

export default DetailNormal
