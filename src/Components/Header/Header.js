import React from 'react'
import Nav from '../Nav/Nav'
import './Header.css'

const Header = () => {
    return (
        <div className="header-wrap">
            <div className="content">
                <Nav />
            </div>
        </div>
    )
}

export default Header;
