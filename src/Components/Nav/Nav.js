import React from 'react'
import { Link } from 'react-router-dom';
import Logo from '../Logo/Logo';
import './Nav.css'

const Nav = () => {

    return (
        <nav className="nav-wrap">
            <Logo />
            <ul>
                <li><Link to="/ContactList">Lista de Contatos</Link></li>
                <li><Link to="/Favorites">Favoritos</Link></li>
                <li><Link to="/Examples">Exemplos</Link></li>
            </ul>
        </nav>
    )
}

export default Nav;
