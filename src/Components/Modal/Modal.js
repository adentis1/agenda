import React from 'react';
import Buttuon from '../Button/Buttuon';
import './Modal.css';

const Modal = ({ onClose, children, title, actions }) => {
    return (
        <div className="modal-wrap" onClick={onClose}>
            <div className="modal" onClick={e => e.stopPropagation()}>
                <header>
                    <h3>{title}</h3>
                </header>
                <div className="modal-wrap_content">
                    {children}
                </div>
                <footer>
                    <Buttuon onClick={onClose}>Cancelar</Buttuon>
                    {actions}
                </footer>
            </div>
        </div>
    )
}

export default Modal
