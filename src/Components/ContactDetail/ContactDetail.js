import React from 'react'
import { useHistory } from 'react-router-dom'
import Buttuon from '../Button/Buttuon';
import DetailNormal from '../Detail/DetailNormal';

const ContactDetail = ({ contact }) => {
    const history = useHistory();
    const handleClick = () => {
        history.goBack();
    }
    const labelStyle = { width: '60px' }
    return (
        <div className="contact-detail-wrap">
            <Buttuon style={{ marginTop: '10px' }} onClick={handleClick}>Voltar</Buttuon>
            <section>
                <DetailNormal labelStyle={labelStyle} label="Nome" value={`${contact.name.first} ${contact.name.last}`} />
                <DetailNormal labelStyle={labelStyle} label="Email" value={contact.email} />

            </section>
        </div >
    )
}

export default ContactDetail
