import React from 'react';
import './IconButton.css';

const IconButton = ({ className, onClick, children, ...rest }) => {
    return (
        <button className={[className, 'btn-icon-wrap'].join(' ')} {...rest} onClick={onClick}>
            {children}
        </button>
    );
}


export default IconButton;