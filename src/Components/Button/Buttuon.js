import React from 'react';
import './Button.css';

export const ButtonType = {
    Default: 'default',
    SUCCESS: 'success',
    ERROR: 'error',
    WARNING: 'warning',
    INFO: 'info'
};

const Buttuon = ({ buttonType = ButtonType.Default, className, onClick, children, ...rest }) => {
    return (
        <button className={[className, 'btn-wrap', `btn-${buttonType}`].join(' ')} {...rest} onClick={onClick}>
            {children}
        </button>
    )
}

export default Buttuon
