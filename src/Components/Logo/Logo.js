import React from 'react'
import { Link } from 'react-router-dom'

const Logo = () => {

    const style = {
        fontSize: '1.5rem',
        padding: '5px 10px',
        border: '1px solid white',
        borderRadius: '8px',
        color: '#fff',

    };

    return (
        <Link to="/" style={style}>Agenda</Link>
    )
}

export default Logo
